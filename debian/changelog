ruby-fxruby (1.6.29-0kali7) kali-dev; urgency=medium

  * Don't run test during the build: xvfb-run is required for the test and it
    fails on i386

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 02 Jul 2019 16:16:11 +0200

ruby-fxruby (1.6.29-0kali6) kali-dev; urgency=medium

  * Don't use xvfb-run in debian/rules (not needed anymore, and it fails on
    i386)
  * Update debian/watch to fix a lintian warning
  * Don't use autopkgtest-pkg-ruby: upstream tests are not run with this
    command. Add a debian/test/control to run upstream test

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 02 Jul 2019 12:23:14 +0200

ruby-fxruby (1.6.29-0kali5) kali-dev; urgency=medium

  * debian/rules: don't run dh_dwz, it fails on fox16_c.so

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 02 Jul 2019 09:39:34 +0200

ruby-fxruby (1.6.29-0kali4) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Update Maintainer field
  * Update Vcs-* fields for the move to gitlab.com

  [ Sophie Brun ]
  * Update add-missing-gemspec.patch
  * Use debhelper-compat 12
  * Update email
  * Bump Standards-Version to 4.3.0
  * Add debian/gbp.conf

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 28 Jun 2019 15:44:35 +0200

ruby-fxruby (1.6.29-0kali3) kali-dev; urgency=medium

  * Update packaging to work with new ruby version 1:2.5~1
  * Drop useless build-deps: ruby-mini-portile2 (only required for most
    recent versions of fxruby but we keep this version for watobo)
  * Simplify add-missing-gemspec.patch
  * Upgrade installation (installation is now managed correctly by dh_ruby):
       - Add a patch installation-with-rakefile.patch
       - Drop useless debian/ruby-fxruby.install
       - debian/rules: don't override dh_auto_install
       - debian/rules: remove /usr/lib/ruby/vendor_ruby/fox16_c.so already 
       in /usr/lib/$(DEB_HOST_MULTIARCH)/ruby/vendor_ruby/$(RUBY_VERSION)

 -- Sophie Brun <sophie@freexian.com>  Thu, 15 Feb 2018 16:54:15 +0100

ruby-fxruby (1.6.29-0kali2) kali-dev; urgency=medium

  * Use xfvb-run to run the tests (some tests fail with "openDisplay: unable to
    open display :0.0")

 -- Sophie Brun <sophie@freexian.com>  Thu, 16 Mar 2017 09:40:22 +0100

ruby-fxruby (1.6.29-0kali1) kali-dev; urgency=medium

  * Initial packaging for kali

 -- Sophie Brun <sophie@freexian.com>  Fri, 26 Aug 2016 10:15:59 +0200
